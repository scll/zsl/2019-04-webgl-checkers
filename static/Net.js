class Net {

    constructor() {
        console.log("Net.js działa")
        // this.handleData()
    }

    addNickname(nickname) {
        $.ajax({
            data: { "action": "addNickname", "nick": nickname },
            type: "POST",
            success: function (data) {
                var obj = JSON.parse(data)
                document.getElementById("status1").innerHTML = obj.status
                localData.nickname = nickname
                localData.players = obj.playersTab
                if (obj.readyToStart == true && obj.correctLogin == true) {
                    board.checkersCreate();
                    localData.playerOrder = "second"
                    net.firstLogin()
                    net.start()
                }
                else if (obj.readyToStart == false && obj.status == "LOGGED_IN") {
                    board.checkersCreate();
                    console.log("oczekiwanie na 2")
                    localData.playerOrder = "first"
                    localData.waiting = setInterval(net.waiting, 500)
                }

            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
    resetNicknames() {
        $.ajax({
            data: { "action": "resetNicknames" },
            type: "POST",
            success: function (data) {
                var obj = JSON.parse(data)
                console.log("players_array.pop()")
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
    waiting() {
        $.ajax({
            data: { "action": "waiting" },
            type: "POST",
            success: function (data) {
                // console.log("waiting")
                var obj = JSON.parse(data)
                localData.players = obj.playersTab
                document.getElementById("status1").innerHTML = obj.status
                if (obj.readyToStart == true) {
                    localData.playerOrder = "first"
                    net.firstLogin()
                    net.start()
                }
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
    start() {
        ui.start()
        clearInterval(localData.waiting)
        game.initRaycast()
        game.playerCamera(localData.playerOrder)
        console.log("gotowy do gry")
    }
    firstLogin() {
        var playerOrd;
        localData.playerOrder == "first" ? playerOrd = "1" : playerOrd = "2"
        $.ajax({
            data: { "action": "firstLogin", "playerOrd": playerOrd, "updatedBoard": JSON.stringify(board.checkers) },
            type: "POST",
            success: function (data) {
                var obj = JSON.parse(data)
                console.log("first login")
                let newTab = JSON.parse(obj.updatedTab)
                game.checkers = newTab
                board.checkersUpdate()
                if (playerOrd == "2") {
                    localData.waiting = setInterval(net.afterTurn, 500)
                }
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
    correctTurn() {
        $.ajax({
            data: { "action": "correctTurn", "updatedBoard": JSON.stringify(board.checkers) },
            type: "POST",
            success: function (data) {
                var obj = JSON.parse(data)
                console.log("wyslano updated tablice na serwer")
                let newTab = JSON.parse(obj.updatedTab)
                game.checkers = newTab
                board.checkersUpdate()
                localData.waiting = setInterval(net.afterTurn, 500)
                document.getElementById("status3").innerHTML = "Ruch przeciwnika"
                document.getElementById("status3").classList = "status oponentsTurn"
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
    additionalTurn() {
        $.ajax({
            data: { "action": "additionalTurn", "updatedBoard": JSON.stringify(board.checkers) },
            type: "POST",
            success: function (data) {
                var obj = JSON.parse(data)
                console.log("dodatkowy ruch")
                document.getElementById("status3").innerHTML = "Dodatkowy ruch!"
                let newTab = JSON.parse(obj.updatedTab)
                game.checkers = newTab
                board.checkersUpdate()
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
    afterTurn() {
        $.ajax({
            data: { "action": "waitingForUpdate", "currentBoard": JSON.stringify(board.checkers) },
            type: "POST",
            success: function (data) {
                var obj = JSON.parse(data)
                // console.log("oczekiwanie na ruch przeciwnika")
                if (obj.updatedTab) {
                    if (obj.zmiana == true && obj.addTurn == false && obj.winner == undefined) {
                        let newTab = JSON.parse(obj.updatedTab)
                        clearInterval(localData.waiting)
                        localData.availableTurn = true
                        game.checkers = newTab
                        board.checkersUpdate(newTab)
                        document.getElementById("status3").innerHTML = "Twoj ruch!"
                        document.getElementById("status3").classList = "status yourTurn"
                    }
                    if (obj.zmiana == true && obj.addTurn == true && obj.winner == undefined) {
                        document.getElementById("status3").innerHTML = "+ Ruch przeciwnika"
                        let newTab = JSON.parse(obj.updatedTab)
                        game.checkers = newTab
                        board.checkersUpdate(newTab)
                    }
                    if (obj.zmiana == true && obj.winner != undefined) {
                        let newTab = JSON.parse(obj.updatedTab)
                        localData.winner = obj.winner
                        localData.intersectObjects = []
                        game.checkers = newTab
                        board.checkersUpdate(newTab)
                        clearInterval(localData.waiting)
                        ui.results()
                        let refresh = confirm("Przegrałeś! Zagrać ponownie?")
                        if (refresh) location.reload()
                    }

                }

            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
    endGame(winner) {
        $.ajax({
            data: { "action": "endGame", "winner": winner, "nick": localData.nickname, "updatedBoard": JSON.stringify(board.checkers) },
            type: "POST",
            success: function (data) {
                var obj = JSON.parse(data)
                localData.winner = obj.winner
                localData.intersectObjects = []
                ui.results()
                let refresh = confirm("Wygrałeś! Zagrać ponownie?")
                if (refresh) location.reload()
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
}
