var net,
    ui,
    game,
    board
$(document).ready(function () {
    net = new Net()
    ui = new UI()
    game = new Game3D()
    board = new Board3D()
    game.init()
})
localData = {
    intersectObjects: [],
    playerColor: undefined,
    playerColorKing: undefined,
    playerColorHex: undefined,
    playerOrder: undefined,
    lastClickedPawn: undefined,
    lastCheckedSpots: [undefined, undefined],
    waiting: undefined,
    nickname: undefined,
    players: undefined,
    availableTurn: undefined,
    popIntersetcs: 0,
    popIntersectsWhite: 0,
    popIntersectsBlack: 0,
    helpPawnsCount: 0,
}