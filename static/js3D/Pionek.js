class Pionek extends THREE.Mesh {
    constructor(geometry, material, nameX) {
        super() // wywołanie konstruktora klasy z której dziedziczymy czyli z Mesha
        this.geometry = geometry
        this.material = material
        this._name = nameX
        this.geometry.radiusTop = 40
        this.geometry.height = 20
    }
    set kolor(val) {
        this.material.color.setHex(val);
    }
    get kolor() {
        return this.material.color
    }
    set name2(val) {
        this._name = val
    }
    get name2() {
        return this._name
    }
}