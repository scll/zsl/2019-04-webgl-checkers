class Square extends THREE.Mesh {
    constructor(geometry, material, nameX) {
        super()
        this.geometry = geometry
        this.material = material
        this._name = nameX
    }
    set kolor(val) {
        this.material.color.setHex(val);
    }
    get kolor() {
        return this.material.color
    }
    set name2(val) {
        this._name = val
    }
    get name2() {
        return this._name
    }
}