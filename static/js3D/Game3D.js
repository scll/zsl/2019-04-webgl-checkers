class Game3D {

    constructor() {
        console.log("Game3D.js działa")
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.TextureLoader().load("imgs/background.png");
        this.camera = new THREE.PerspectiveCamera(45, $("#root").width() / $("#root").height(), 0.1, 10000)
        this.camera.position.set(0, 1800, 0)
        this.renderer = new THREE.WebGLRenderer({ antialias: true });
        this.renderer.setClearColor(0x222222, 1)
        this.renderer.setSize($("#root").width(), $("#root").height());
        this.orbitControl = new THREE.OrbitControls(this.camera, this.renderer.domElement);
        this.axes = new THREE.AxesHelper(1000)
        // this.scene.add(this.axes)
        $("#root").append(this.renderer.domElement);

        this.raycaster = new THREE.Raycaster()
        this.mouseVector = new THREE.Vector2()
    }

    render() {
        requestAnimationFrame(game.render);
        game.renderer.render(game.scene, game.camera);
    }

    orbitControls() {
        game.orbitControl.addEventListener('change', function () {
            game.renderer.render(game.scene, game.camera)
        });
    }
    windowResize() {
        window.addEventListener('resize', function () {
            game.camera.aspect = $("#root").innerWidth() / $("#root").innerHeight();
            game.camera.updateProjectionMatrix();
            game.renderer.setSize($("#root").innerWidth(), $("#root").innerHeight());
        })
    }
    playerCamera(player) {
        if (player == "first") {
            game.camera.position.set(0, 1500, 1000)
            game.orbitControl = new THREE.OrbitControls(game.camera, game.renderer.domElement);
        }
        else if (player == "second") {
            game.camera.position.set(0, 1500, -1000)
            game.orbitControl = new THREE.OrbitControls(game.camera, game.renderer.domElement);
        }
    }
    click(event) {
        this.mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
        this.mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
        this.raycaster.setFromCamera(this.mouseVector, this.camera);
        let intersects = this.raycaster.intersectObjects(localData.intersectObjects); //true - mozliwosc "klikania" dzieci dzieci sceny
        if (intersects.length > 0) {
            console.log(intersects[0].object)
            if (localData.availableTurn == true) {
                var plus1;
                var plus2;
                var replaced;
                var replacedKing;
                var kingY;
                var zbijanyPionek;
                var zbijanyKrol;
                var pawnsCount;
                localData.playerOrder == "first" ? plus1 = -1 : plus1 = 1
                localData.playerOrder == "first" ? plus2 = -2 : plus2 = 2
                localData.playerOrder == "first" ? replaced = 1 : replaced = 2
                localData.playerOrder == "first" ? replacedKing = 3 : replacedKing = 4
                localData.playerOrder == "first" ? kingY = 0 : kingY = 7
                localData.playerOrder == "first" ? zbijanyPionek = 2 : zbijanyPionek = 1
                localData.playerOrder == "first" ? zbijanyKrol = 4 : zbijanyKrol = 3
                localData.playerOrder == "first" ? pawnsCount = localData.popIntersectsWhite : pawnsCount = localData.popIntersectsBlack

                if (localData.lastClickedPawn) localData.lastClickedPawn.kolor = localData.playerColorHex

                if (intersects[0].object.name2 == localData.playerColor || intersects[0].object.name2 == localData.playerColorKing) {
                    localData.lastClickedColor = intersects[0].object.kolor
                    localData.lastClickedPawn = intersects[0].object
                    let kolor;
                    localData.playerColor == "whitePawn" ? kolor = "0xcd853e" : kolor = "0xffffff"
                    intersects[0].object.kolor = kolor

                    let currentX = parseInt(localData.lastClickedPawn.name[2])
                    let currentY = parseInt(localData.lastClickedPawn.name[0])
                    if (board.checkers[currentY + plus1][currentX + 1] == 0) {
                        board.defaultBoard3D[currentY + plus1][currentX + 1].kolor = "0x9df9ca"
                        localData.lastCheckedSpots[0] = board.defaultBoard3D[currentY + plus1][currentX + 1]
                    }
                    else if (board.checkers[currentY + plus1][currentX + 1] == 1 && board.checkers[currentY + plus2][currentX + 2] == 0) {
                        board.defaultBoard3D[currentY + plus2][currentX + 2].kolor = "0x9df9ca"
                        localData.lastCheckedSpots[0] = board.defaultBoard3D[currentY + plus2][currentX + 2]
                    }
                    if (board.checkers[currentY + plus1][currentX - 1] == 0) {
                        board.defaultBoard3D[currentY + plus1][currentX - 1].kolor = "0x9df9ca"
                        localData.lastCheckedSpots[1] = board.defaultBoard3D[currentY + plus1][currentX - 1]
                    }
                    else if (board.checkers[currentY + plus1][currentX - 1] == 1 && board.checkers[currentY + plus2][currentX - 2] == 0) {
                        board.defaultBoard3D[currentY + plus2][currentX - 2].kolor = "0x9df9ca"
                        localData.lastCheckedSpots[0] = board.defaultBoard3D[currentY + plus2][currentX - 2]
                    }

                }
                if (intersects[0].object != localData.lastClickedPawn) {
                    if (localData.lastCheckedSpots[0]) { localData.lastCheckedSpots[0].kolor = "0xdddddd" }
                    if (localData.lastCheckedSpots[1]) { localData.lastCheckedSpots[1].kolor = "0xdddddd" }
                    if (intersects[0].object.parent.name == "whiteSpot" && localData.lastClickedPawn) {
                        let currentX = parseInt(localData.lastClickedPawn.name[2])
                        let currentY = parseInt(localData.lastClickedPawn.name[0])
                        let clickX = parseInt(intersects[0].object.name[2])
                        let clickY = parseInt(intersects[0].object.name[0])
                        // console.log(clickY, clickX)

                        if (localData.lastClickedPawn.name2 == localData.playerColor) { //ruch pionkiem
                            if ((clickY == currentY + plus1 && clickX == currentX + 1) || (clickY == currentY + plus1 && clickX == currentX - 1)) {
                                if (board.checkers[clickY][clickX] == 0) {
                                    localData.lastClickedPawn.position.x = intersects[0].object.clone().position.x
                                    localData.lastClickedPawn.position.z = intersects[0].object.clone().position.z
                                    localData.lastClickedPawn.name = clickY + "p" + clickX
                                    board.checkers[currentY][currentX] = 0
                                    board.checkers[clickY][clickX] = replaced
                                    if (clickY == kingY) board.checkers[clickY][clickX] = replacedKing
                                    localData.lastClickedPawn = undefined
                                    localData.availableTurn = false
                                    net.correctTurn()
                                }
                            }
                            if (clickY == currentY + plus2 && clickX == currentX + 2) {
                                let between1 = board.checkers[currentY + plus1][currentX + 1]
                                if (board.checkers[clickY][clickX] == 0 && (between1 == zbijanyPionek || between1 == zbijanyKrol)) {
                                    localData.lastClickedPawn.position.x = intersects[0].object.clone().position.x
                                    localData.lastClickedPawn.position.z = intersects[0].object.clone().position.z
                                    localData.lastClickedPawn.name = clickY + "p" + clickX
                                    board.checkers[currentY][currentX] = 0
                                    board.checkers[clickY][clickX] = replaced
                                    if (clickY == kingY) board.checkers[clickY][clickX] = replacedKing
                                    board.checkers[currentY + plus1][currentX + 1] = 0
                                    localData.lastClickedPawn = undefined
                                    // localData.availableTurn = false
                                    if (pawnsCount > 1) { net.additionalTurn() }
                                    else if (pawnsCount == 1) {
                                        if ((board.checkers[clickY + plus1][clickX + 1] == 0) || (board.checkers[clickY + plus1][clickX - 1] == 0)) {
                                            net.additionalTurn()
                                        }
                                        else {
                                            net.correctTurn()
                                        }
                                    }
                                }
                            }
                            if (clickY == currentY + plus2 && clickX == currentX - 2) {
                                let between2 = board.checkers[currentY + plus1][currentX - 1]
                                if (board.checkers[clickY][clickX] == 0 && (between2 == zbijanyPionek || between2 == zbijanyKrol)) {
                                    localData.lastClickedPawn.position.x = intersects[0].object.clone().position.x
                                    localData.lastClickedPawn.position.z = intersects[0].object.clone().position.z
                                    localData.lastClickedPawn.name = clickY + "p" + clickX
                                    board.checkers[currentY][currentX] = 0
                                    board.checkers[clickY][clickX] = replaced
                                    if (clickY == kingY) board.checkers[clickY][clickX] = replacedKing
                                    board.checkers[currentY + plus1][currentX - 1] = 0
                                    localData.lastClickedPawn = undefined
                                    // localData.availableTurn = false
                                    if (pawnsCount > 1) { net.additionalTurn() }
                                    else if (pawnsCount == 1) {
                                        if ((board.checkers[clickY + plus1][clickX + 1] == 0) || (board.checkers[clickY + plus1][clickX - 1] == 0)) {
                                            net.additionalTurn()
                                        }
                                        else {
                                            net.correctTurn()
                                        }
                                    }
                                }
                            }
                        }
                        else if (localData.lastClickedPawn.name2 == localData.playerColorKing) { //ruch królową
                            console.log("krol")
                            if (Math.abs(currentX - clickX) == Math.abs(currentY - clickY)) {
                                let plusX = 0
                                let plusY = 0
                                let lastPawn;
                                localData.helpPawnsCount = 0
                                for (let k = 0; k < Math.abs(currentX - clickX); k++) {
                                    (currentX - clickX) > 0 ? plusX-- : plusX++
                                    (currentY - clickY) > 0 ? plusY-- : plusY++
                                    if (board.checkers[currentY + plusY][currentX + plusX] != 0) {
                                        console.log("ilosc zajetych")
                                        localData.helpPawnsCount++
                                        lastPawn = [(currentY + plusY), (currentX + plusX)]
                                    }
                                }
                                if (board.checkers[clickY][clickX] == 0) {
                                    let plus3 = 0;
                                    let plus4 = 0;
                                    (currentY - clickY) > 0 ? plus3++ : plus3--
                                    (currentX - clickX) > 0 ? plus4++ : plus4--
                                    // console.log(plus3)
                                    // console.log(plus4)
                                    // console.log(localData.helpPawnsCount)
                                    // console.log(lastPawn[0])
                                    // console.log(lastPawn[1])
                                    // console.log(clickY + plus3)
                                    // console.log(clickX + plus4)
                                    let between3 = board.checkers[clickY + plus3][clickX + plus4]
                                    if (localData.helpPawnsCount == 1 && clickY + plus3 == lastPawn[0] && clickX + plus4 == lastPawn[1] && (between3 == zbijanyPionek || between3 == zbijanyKrol)) {
                                        localData.lastClickedPawn.position.x = intersects[0].object.clone().position.x
                                        localData.lastClickedPawn.position.z = intersects[0].object.clone().position.z
                                        localData.lastClickedPawn.name = clickY + "p" + clickX
                                        board.checkers[currentY][currentX] = 0
                                        board.checkers[clickY][clickX] = replacedKing
                                        board.checkers[clickY + plus3][clickX + plus4] = 0
                                        localData.lastClickedPawn = undefined
                                        // localData.availableTurn = false
                                        net.additionalTurn()

                                    }
                                    else if (localData.helpPawnsCount == 0) {
                                        localData.lastClickedPawn.position.x = intersects[0].object.clone().position.x
                                        localData.lastClickedPawn.position.z = intersects[0].object.clone().position.z
                                        localData.lastClickedPawn.name = clickY + "p" + clickX
                                        board.checkers[currentY][currentX] = 0
                                        board.checkers[clickY][clickX] = replacedKing
                                        localData.lastClickedPawn = undefined
                                        localData.availableTurn = false
                                        net.correctTurn()
                                    }
                                }
                            }
                            // for (let k = 0; k < 8; k++) {
                            //     let plus3 = -k
                            //     if ((clickY == currentY + plus3 && clickX == currentX + k) || (clickY == currentY + plus3 && clickX == currentX - k)) {
                            //         if (board.checkers[clickY][clickX] == 0) {
                            //             localData.lastClickedPawn.position.x = intersects[0].object.clone().position.x
                            //             localData.lastClickedPawn.position.z = intersects[0].object.clone().position.z
                            //             localData.lastClickedPawn.name = clickY + "p" + clickX
                            //             board.checkers[currentY][currentX] = 0
                            //             board.checkers[clickY][clickX] = replacedKing
                            //             localData.lastClickedPawn = undefined
                            //             localData.availableTurn = false
                            //             net.correctTurn()
                            //         }
                            //     }
                            //     let plus4 = k
                            //     if ((clickY == currentY + plus4 && clickX == currentX + k) || (clickY == currentY + plus4 && clickX == currentX - k)) {
                            //         if (board.checkers[clickY][clickX] == 0) {
                            //             localData.lastClickedPawn.position.x = intersects[0].object.clone().position.x
                            //             localData.lastClickedPawn.position.z = intersects[0].object.clone().position.z
                            //             localData.lastClickedPawn.name = clickY + "p" + clickX
                            //             board.checkers[currentY][currentX] = 0
                            //             board.checkers[clickY][clickX] = replacedKing
                            //             localData.lastClickedPawn = undefined
                            //             localData.availableTurn = false
                            //             net.correctTurn()
                            //         }
                            //     }
                            //     // if()
                            // }
                        }

                    }
                    else if (localData.lastClickedPawn) {
                        localData.lastClickedPawn = undefined
                    }
                }
            }
        }
    }
    initRaycast() {
        const that = this
        $(document).off("mousedown")
        $(document).on("mousedown", function () {
            that.click(event)
        })
    }
    init() {
        game.render()
        game.orbitControls()
        game.windowResize()
        board.boardCreate()
        board.boardSpecials()
    }
}
