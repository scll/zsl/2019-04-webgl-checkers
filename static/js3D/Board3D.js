class Board3D {

    constructor() {
        console.log("Board3D.js działa")
        // this.board = new THREE.Object3D();
        this.squareGeo = new THREE.BoxGeometry(100, 100, 100);
        this.checkerGeo = new THREE.CylinderGeometry(40, 40, 20, 32);
        this.defaultBoard = [
            [1, 0, 1, 0, 1, 0, 1, 0],
            [0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0],
            [0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0],
            [0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0],
            [0, 1, 0, 1, 0, 1, 0, 1],
        ];
        this.checkers = [
            [0, 2, 0, 2, 0, 2, 0, 2],
            [2, 0, 2, 0, 2, 0, 2, 0],
            [0, 2, 0, 2, 0, 2, 0, 2],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [1, 0, 1, 0, 1, 0, 1, 0],
            [0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0],
        ];
        this.defaultBoard3D = []
        this.checkers3D = []
        // this.whiteGroup = new THREE.Group();
        // this.whiteGroup.name = "whitePawn"
        // this.blackGroup = new THREE.Group();
        // this.blackGroup.name = "blackPawn"

        this.whiteSpotsGroup = new THREE.Group();
        this.whiteSpotsGroup.name = "whiteSpot"
        this.blackSpotsGroup = new THREE.Group();
        this.blackSpotsGroup.name = "blackSpot"
        this.addonsGroup = new THREE.Group();
        this.addonsGroup.name = "addons"
    }
    boardCreate() {
        for (let j = 0; j < board.defaultBoard.length; j++) {
            board.defaultBoard3D.push([])
        }
        for (let i = 0; i < board.defaultBoard.length; i++) {
            for (let j = 0; j < board.defaultBoard[i].length; j++) {
                if (board.defaultBoard[i][j] == 1) {
                    let squareMaterial1 = new THREE.MeshPhongMaterial({
                        shininess: 20,
                        side: THREE.DoubleSide,
                        map: new THREE.TextureLoader().load('/imgs/1.png')
                    });
                    let square = new Square(board.squareGeo, squareMaterial1, "blackSpot")
                    square.name = i + "s" + j//"blackSpot"
                    square.position.z = -400 + 100 * i + 50
                    square.position.x = -400 + 100 * j + 50
                    // console.log(game.scene.add(square))
                    game.scene.add(square)
                    board.blackSpotsGroup.add(square)
                    localData.intersectObjects.push(square)
                    board.defaultBoard3D[i].push(square)
                }
                else {
                    let squareMaterial2 = new THREE.MeshPhongMaterial({
                        color: 0xdddddd,
                        shininess: 20,
                        side: THREE.DoubleSide,
                        map: new THREE.TextureLoader().load('/imgs/2.png')
                    });
                    let square = new Square(board.squareGeo, squareMaterial2, "whiteSpot")
                    square.name = i + "s" + j//"whiteSpot"
                    square.position.z = -400 + 100 * i + 50
                    square.position.x = -400 + 100 * j + 50
                    // console.log(game.scene.add(square))
                    board.whiteSpotsGroup.add(square)
                    localData.intersectObjects.push(square)
                    board.defaultBoard3D[i].push(square)
                }
            }
        }
        game.scene.add(board.blackSpotsGroup)
        game.scene.add(board.whiteSpotsGroup)
    }
    checkersCreate(newTab) {
        if (newTab) board.checkers = newTab
        localData.popIntersects = 0
        localData.popIntersectsWhite = 0
        localData.popIntersectsBlack = 0
        for (let i = 0; i < board.checkers.length; i++) {
            for (let j = 0; j < board.checkers[i].length; j++) {
                if (board.checkers[i][j] == 1) {
                    let checkerMaterial2 = new THREE.MeshPhongMaterial({
                        shininess: 100,
                        color: 0xaaaaaa,
                        side: THREE.DoubleSide,
                        map: new THREE.TextureLoader().load('/imgs/2.png')
                    });
                    let pawn = new Pionek(board.checkerGeo, checkerMaterial2, "whitePawn")
                    pawn.name = i + "p" + j//"whitePawn"
                    pawn.position.z = -400 + 100 * i + 50
                    pawn.position.x = -400 + 100 * j + 50
                    pawn.position.y = 50
                    game.scene.add(pawn)
                    board.checkers3D.push(pawn)
                    localData.intersectObjects.push(pawn)
                    localData.popIntersectsWhite++
                }
                else if (board.checkers[i][j] == 2) {
                    let checkerMaterial1 = new THREE.MeshPhongMaterial({
                        shininess: 100,
                        color: 0x777777,
                        side: THREE.DoubleSide,
                        map: new THREE.TextureLoader().load('/imgs/1.png')
                    });
                    let pawn = new Pionek(board.checkerGeo, checkerMaterial1, "blackPawn")
                    pawn.name = i + "p" + j//"blackPawn"
                    pawn.position.z = -400 + 100 * i + 50
                    pawn.position.x = -400 + 100 * j + 50
                    pawn.position.y = 50
                    game.scene.add(pawn)
                    board.checkers3D.push(pawn)
                    localData.intersectObjects.push(pawn)
                    localData.popIntersectsBlack++
                }
                else if (board.checkers[i][j] == 3) {
                    let checkerMaterial2 = new THREE.MeshPhongMaterial({
                        shininess: 100,
                        color: 0xaaaaaa,
                        side: THREE.DoubleSide,
                        map: new THREE.TextureLoader().load('/imgs/2.png')
                    });
                    let pawn = new Pionek(board.checkerGeo, checkerMaterial2, "whitePawnKing")
                    pawn.scale.set(1.2, 1.5, 1.2)
                    pawn.name = i + "p" + j//"whitePawn"
                    pawn.position.z = -400 + 100 * i + 50
                    pawn.position.x = -400 + 100 * j + 50
                    pawn.position.y = 50
                    game.scene.add(pawn)
                    board.checkers3D.push(pawn)
                    localData.intersectObjects.push(pawn)
                    localData.popIntersectsWhite++
                }
                else if (board.checkers[i][j] == 4) {
                    let checkerMaterial2 = new THREE.MeshPhongMaterial({
                        shininess: 100,
                        color: 0xaaaaaa,
                        side: THREE.DoubleSide,
                        map: new THREE.TextureLoader().load('/imgs/1.png')
                    });
                    let pawn = new Pionek(board.checkerGeo, checkerMaterial2, "blackPawnKing")
                    pawn.scale.set(1.2, 1.5, 1.2)
                    pawn.name = i + "p" + j//"blackPawn"
                    pawn.position.z = -400 + 100 * i + 50
                    pawn.position.x = -400 + 100 * j + 50
                    pawn.position.y = 50
                    game.scene.add(pawn)
                    board.checkers3D.push(pawn)
                    localData.intersectObjects.push(pawn)
                    localData.popIntersectsBlack++
                }
            }
        }
        localData.popIntersects = localData.popIntersectsBlack + localData.popIntersectsWhite
        if (localData.popIntersectsWhite == 0 && localData.winner == undefined) net.endGame(1)
        if (localData.popIntersectsBlack == 0 && localData.winner == undefined) net.endGame(0)
    }
    checkersUpdate(newTab) {
        console.log("checkers update")
        for (let i = 0; i < board.checkers3D.length; i++) {
            game.scene.remove(board.checkers3D[i]);
        }
        for (let i = 0; i < localData.popIntersects; i++) {
            localData.intersectObjects.pop()
        }
        board.checkers3D = []
        // board.whiteGroup = new THREE.Group();
        // board.whiteGroup.name = "whitePawn"
        // board.blackGroup = new THREE.Group();
        // board.blackGroup.name = "blackPawn"
        board.checkersCreate(newTab)
        game.initRaycast()

    }
    boardSpecials() {
        let ambientLight = new THREE.AmbientLight(0x404040)
        board.addonsGroup.add(ambientLight)
        let torchMats = []
        torchMats.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('/imgs/frontTorch.png') }));
        torchMats.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('/imgs/frontTorch.png') }));
        torchMats.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('/imgs/topTorch.png') }));
        torchMats.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('/imgs/bottomTorch.png') }));
        torchMats.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('/imgs/frontTorch.png') }));
        torchMats.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('/imgs/frontTorch.png') }));
        let torchGeo = new THREE.BoxGeometry(17, 80, 17)
        let wallMats = []
        wallMats.push(new THREE.MeshPhongMaterial({ shininess: 0, side: THREE.DoubleSide, map: new THREE.TextureLoader().load('/imgs/wall1.png') }));
        wallMats.push(new THREE.MeshPhongMaterial({ shininess: 0, side: THREE.DoubleSide, map: new THREE.TextureLoader().load('/imgs/wall1.png') }));
        wallMats.push(new THREE.MeshPhongMaterial({ shininess: 0, side: THREE.DoubleSide, map: new THREE.TextureLoader().load('/imgs/wall2.png') }));
        wallMats.push(new THREE.MeshPhongMaterial({ shininess: 0, side: THREE.DoubleSide, map: new THREE.TextureLoader().load('/imgs/wall2.png') }));
        wallMats.push(new THREE.MeshPhongMaterial({ shininess: 0, side: THREE.DoubleSide, map: new THREE.TextureLoader().load('/imgs/wall1.png') }));
        wallMats.push(new THREE.MeshPhongMaterial({ shininess: 0, side: THREE.DoubleSide, map: new THREE.TextureLoader().load('/imgs/wall1.png') }));
        let wallGeo = new THREE.BoxGeometry(50, 100, 50)
        for (let i = 0; i < 2; i++) {
            for (let j = 0; j < 2; j++) {
                let wall = new THREE.Mesh(wallGeo, wallMats)
                wall.position.set((-425 + 850 * i), 0, (-425 + 850 * j));
                board.addonsGroup.add(wall)
                let torch = new THREE.Mesh(torchGeo, torchMats)
                torch.position.set((-425 + 850 * i), 80, (-425 + 850 * j));
                board.addonsGroup.add(torch)
                let light = new THREE.SpotLight(0xffddaa, 2, 800, (Math.PI));
                light.position.set((-425 + 850 * i), 230, (-425 + 850 * j));
                board.addonsGroup.add(light)
                let lightSide = new THREE.SpotLight(0xffddaa, 2, 300, (Math.PI));
                lightSide.position.set((-550 + 1100 * i), 130, (-550 + 1100 * j));
                board.addonsGroup.add(lightSide)
            }
        }
        game.scene.add(board.addonsGroup)
    }
}