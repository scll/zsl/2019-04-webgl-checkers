class UI {

    constructor() {
        console.log("UI.js działa")
        this.clicks()
    }

    clicks() {
        $("#login1").on("click", function () {
            const nickname = $("#nick1").val();
            if (nickname != "") {
                net.addNickname(nickname)
            }
        })
        $("#reset1").on("click", function () {
            net.resetNicknames()
        })
    }
    start() {
        var kolor;
        var oponent;
        var turn;
        var turnClass;
        localData.playerOrder == "first" ? kolor = "bialymi" : kolor = "czarnymi"
        localData.playerOrder == "first" ? oponent = 1 : oponent = 0
        localData.playerOrder == "first" ? localData.playerColor = "whitePawn" : localData.playerColor = "blackPawn"
        localData.playerOrder == "first" ? localData.playerColorKing = "whitePawnKing" : localData.playerColorKing = "blackPawnKing"
        localData.playerOrder == "first" ? localData.playerColorHex = "0xaaaaaa" : localData.playerColorHex = "0x777777"
        localData.playerOrder == "first" ? localData.availableTurn = true : localData.availableTurn = false
        localData.playerOrder == "first" ? turn = "Twoj ruch!" : turn = "Ruch przeciwnika"
        localData.playerOrder == "first" ? turnClass = "status yourTurn" : turnClass = "status oponentsTurn"
        document.getElementById("status1").innerHTML = "LOGGED_IN"
        document.getElementById("status2").innerHTML = "Witaj, " + localData.nickname + ", grasz " + kolor + ", twoj przeciwnik: " + localData.players[oponent]
        document.getElementById("status3").innerHTML = turn
        document.getElementById("status3").classList = turnClass
        document.getElementById("overlay").style.visibility = "hidden"
    }
    results() {
        document.getElementById("status1").style.visibility = "hidden"
        document.getElementById("status2").style.visibility = "hidden"
        document.getElementById("status3").style.visibility = "hidden"
        document.getElementById("status4").innerHTML = "Wygrywa gracz " + localData.winner
    }
}
