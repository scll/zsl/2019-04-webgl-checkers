var http = require("http");
var qs = require("querystring");
var fs = require("fs")


var server = http.createServer(function (req, res) {
    // console.log(req.method) // zauważ ze przesyłane po kliknięciu butona dane, będą typu POST
    // console.log(req.url)
    switch (req.method) {
        case "GET":
            if (req.url == "/") {
                fs.readFile("static/index.html", function (error, data) {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.write(data);
                    res.end();
                })
            }
            else if (req.url.indexOf(".css") != -1) {
                fs.readFile("static/" + decodeURI(req.url), function (error, data) {
                    res.writeHead(200, { 'Content-Type': 'text/css' });
                    res.write(data);
                    res.end();
                })
            }
            else if (req.url.indexOf(".js") != -1) {
                fs.readFile("static/" + decodeURI(req.url), function (error, data) {
                    res.writeHead(200, { "Content-type": "application/javascript" });
                    res.write(data);
                    res.end();
                })
            }
            else if (req.url.indexOf(".mp3") != -1) {
                fs.readFile("static/" + decodeURI(req.url), function (error, data) {
                    res.writeHead(200, { "Content-type": "audio/mp3" });
                    res.write(data);
                    res.end();
                })
            }
            else if (req.url.indexOf(".jpg") != -1) {
                fs.readFile("static/" + decodeURI(req.url), function (error, data) {
                    res.writeHead(200, { "Content-type": "image/jpg" });
                    res.write(data);
                    res.end();
                })
            }
            else if (req.url.indexOf(".png") != -1) {
                fs.readFile("static/" + decodeURI(req.url), function (error, data) {
                    res.writeHead(200, { "Content-type": "image/png" });
                    res.write(data);
                    res.end();
                })
            }
            else if (req.url.indexOf(".ttf") != -1) {
                fs.readFile("static/" + decodeURI(req.url), function (error, data) {
                    res.writeHead(200, { "Content-type": "application/octet-stream" });
                    res.write(data);
                    res.end();
                })
            }
            break;
        case "POST":
            servResponse(req, res)
            break;
    }
})
var players = []
var checkers = undefined
var addTurn = false
var winner = undefined
var servResponse = function (req, res) {
    var allData = "";
    req.on("data", function (data) {
        allData += data
    })
    req.on("end", function (data) {
        var finish = qs.parse(allData)
        var response = {
            action: finish.action,
            correctLogin: undefined,
            status: undefined,
            playerOrder: undefined,
            readyToStart: false,
            playersTab: players,
            updatedTab: checkers,
            zmiana: undefined,
            addTurn: addTurn,
            winner: winner,
        }
        switch (finish.action) {
            case "addNickname":
                if (players.length < 2) {
                    if (players[0] != finish.nick) {
                        players.length == 0 ? response.playerOrder = "first" : response.playerOrder = "second"
                        players.push(finish.nick)
                        response.status = "LOGGED_IN"
                        response.correctLogin = true
                        if (players.length == 2) { response.readyToStart = true }
                    }
                    else {
                        response.status = "USER_EXISTS"
                    }
                }
                else {
                    response.status = "LOBBY_FULL"
                }
                break;
            case "resetNicknames":
                players.pop()
                break;
            case "waiting":
                response.status = "WAITING_FOR_OPONENT"
                if (players.length == 2) { response.readyToStart = true }
                break;
            case "firstLogin":
                checkers = finish.updatedBoard
                response.updatedTab = checkers
                winner = undefined
                break;
            case "correctTurn":
                checkers = finish.updatedBoard
                response.updatedTab = checkers
                break;
            case "additionalTurn":
                checkers = finish.updatedBoard
                response.updatedTab = checkers
                addTurn = true
                response.addTurn = addTurn
                break;
            case "endGame":
                checkers = finish.updatedBoard
                response.updatedTab = checkers
                addTurn = false
                winner = players[finish.winner]
                response.winner = winner
                break;
            case "waitingForUpdate":
                if (checkers != finish.currentBoard && checkers && winner == undefined) {
                    response.zmiana = true
                    response.updatedTab = checkers
                    response.addTurn = addTurn
                    addTurn = false
                }
                else if (winner) {
                    response.zmiana = true
                    response.updatedTab = checkers
                    response.addTurn = addTurn
                    response.winner = winner
                }
                break;
        }
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end(JSON.stringify(response));
    })
}

server.listen(4000, function () {
    console.log("serwer startuje na porcie 4000")
});